import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';

@Component( {
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.sass']
} )
export class SigninComponent implements OnInit {
  signinForm: FormGroup;

  constructor( private formBuilder: FormBuilder, private authService: AuthService ) {
  }

  ngOnInit() {
    this.signinForm = this.formBuilder.group( {
      name: new FormControl( '', Validators.required ),
      password: new FormControl( '', Validators.required ),
    } );
  }

  private getFormData(): any {
    return this.signinForm.value;
  }

  login(): void {
    const {name, password} = this.getFormData();
    this.authService.login( name, password );
  }
}
