import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';

@Component( {
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
} )
export class SignupComponent implements OnInit {
  signupForm: FormGroup;

  constructor( private formBuilder: FormBuilder, private authService: AuthService ) {
  }

  ngOnInit() {
    this.signupForm = this.formBuilder.group( {
      name: new FormControl( '', Validators.required ),
      email: new FormControl( '', [Validators.required, Validators.email] ),
      password: new FormControl( '', Validators.required ),
    } );
  }

  private getFormData(): any {
    return this.signupForm.value;
  }

  register(): void {
    const {name, email, password} = this.getFormData();
    this.authService.register( name, email, password );
  }
}
