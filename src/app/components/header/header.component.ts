import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';

@Component( {
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
} )
export class HeaderComponent implements OnInit {

  constructor( private authService: AuthService ) {
  }

  ngOnInit() {
  }

  isAuth() {
    return this.authService.isAuth();
  }

  getUsername() {
    return this.authService.getUsername();
  }

  logout() {
    this.authService.logout();
  }

}
