import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable( {
  providedIn: 'root'
} )
export class AuthService {
  private url = 'http://10.1.0.60:3001/auth/';
  private accessToken: string;
  private refreshToken: string;
  private username: string;

  constructor( private httpClient: HttpClient, private router: Router ) {
    const access = window.localStorage.getItem( 'accessToken' );
    const refresh = window.localStorage.getItem( 'refreshToken' );
    if ( access && refresh ) {
      this.accessToken = access;
      this.refreshToken = refresh;
      this.username = window.localStorage.getItem( 'username' );
    }
  }

  login( name: string, password: string ): void {
    const body = {name, password};
    this.httpClient.post( `${ this.url }authenticate`, body ).subscribe(
      ( info: any ) => {
        this.accessToken = info.accessToken.jwtToken;
        this.username = info.accessToken.payload.username;
        this.refreshToken = info.refreshToken.token;
        this.router.navigate( [''] );
        this.addTokens();
      },
      error => {
        console.error( error );
      }
    );
  }

  register( name: string, email: string, password: string ) {
    const body = {name, email, password};
    this.httpClient.post( `${ this.url }register`, body ).subscribe(
      value => console.log( value ),
      error => console.error( error )
    );
  }

  logout(): void {
    delete this.refreshToken;
    delete this.accessToken;
    this.removeTokens();
  }

  isAuth() {
    return this.accessToken && this.refreshToken;
  }

  getUsername(): string {
    return this.username;
  }

  private removeTokens() {
    window.localStorage.removeItem( 'accessToken' );
    window.localStorage.removeItem( 'refreshToken' );
    window.localStorage.removeItem( 'username' );
  }

  private addTokens() {
    window.localStorage.setItem( 'accessToken', this.accessToken );
    window.localStorage.setItem( 'refreshToken', this.refreshToken );
    window.localStorage.setItem( 'username', this.username );
  }

}
